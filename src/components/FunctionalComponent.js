import React from 'react';

const greet = () => <h1>hello Yanu arrow function</h1>;
/*
const greet = () => <h1>hello Yanu arrow function</h1>;

saya bisa membuat kesimpulan sendiri bahwa arrow function di atas sebenarnya sama
dengan function di bawah:

function greet(){
    return <h1>hello Yanu not arrow function</h1>;
}
*/

export default greet