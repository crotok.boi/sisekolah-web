import React, { Component } from 'react';
import './App.css';
import Greet from './components/FunctionalComponent'
import ClassComponent from './components/ClassComponent'

class App extends Component {
  render() {
    return (
      <div className="App">
          <h2>Welcome to React</h2>
          <Greet/>
          <ClassComponent/>
      </div>
    );
  }
}

export default App;
